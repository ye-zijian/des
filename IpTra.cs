﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyDES
{
    public class IpTra
    {
        private int[,] IP = new int[8, 8]
        {
            {58,50,42,34,26,18,10,2 },
            {60,52,44,36,28,20,12,4 },
            {62,54,46,38,30,22,14,6 },
            {64,56,48,40,32,24,16,8 },
            {57,49,41,33,25,17,9 ,1 },
            {59,51,43,35,27,19,11,3 },
            {61,53,45,37,29,21,13,5 },
            {63,55,47,39,31,23,15,7 }
        };

        private int[,] Text_64Bit = new int[8, 8];

        private int[,] DisText_64Bit = new int[8, 8];

        private int[,] Dop_Left_32Bit = new int[4, 8];

        private int[,] Dop_Right_32Bit = new int[4, 8];

        public IpTra(int[,] text_64Bit)
        {
            this.Text_64Bit = text_64Bit;
            this.GetIpTraVal();
        }

        private int[,] GetIpTraVal()
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    int row = (IP[i, j] - 1) / 8;
                    int col = (IP[i, j] - 1) % 8;
                    this.DisText_64Bit[i, j] = this.Text_64Bit[row, col];
                }
            }

            return this.DisText_64Bit;
        }

        public int[,] GetDop_Left_32Bit()
        {
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    this.Dop_Left_32Bit[i, j] = this.DisText_64Bit[i, j];
                }
            }

            return this.Dop_Left_32Bit;
        }

        public int[,] GetDop_Right_32Bit()
        {
            for (int i = 4; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    this.Dop_Right_32Bit[i - 4, j] = this.DisText_64Bit[i, j];
                }
            }

            return this.Dop_Right_32Bit;
        }
    }
}
