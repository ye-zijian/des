﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyDES
{
    public class RoundKeyEnc_Pc_2
    {
        private int[,] Key_Input_56Bit = new int[8, 7];

        private int[,] Key_Output_48Bit = new int[8, 6];

        private int[,] Pc_2 = new int[8, 6]
        {
            {14,17,11,24,1 ,5  },
            {3 ,28,15,6 ,21,10 },
            {23,19,12,4 ,26,8  },
            {16,7 ,27,20,13,2  },
            {41,52,31,37,47,55 },
            {30,40,51,45,33,48 },
            {44,49,39,56,34,53 },
            {46,42,50,36,29,32 }
        };

        public RoundKeyEnc_Pc_2(int[,] key_Left_28Bit, int[,] key_Right_28Bit)
        {
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 7; j++)
                {
                    this.Key_Input_56Bit[i, j] = key_Left_28Bit[i, j];
                    this.Key_Input_56Bit[i + 4, j] = key_Right_28Bit[i, j];
                }
            }
        }

        public int[,] GetKey_48Bit()
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 6; j++)
                {
                    int row = (Pc_2[i, j] - 1) / 7;
                    int col = (Pc_2[i, j] - 1) % 7;
                    this.Key_Output_48Bit[i, j] = this.Key_Input_56Bit[row, col];
                }
            }

            return Key_Output_48Bit;
        }
    }
}
