﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyDES
{
    public class ExTra
    {
        private int[,] Input_32Bit = new int[8, 4];

        private int[,] Output_48Bit = new int[8, 6];

        public ExTra(int[,] input_32Bit)
        {
            this.Input_32Bit = input_32Bit;
        }

        public int[,] Extend()
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 6; j++)
                {
                    if (((i * 6 + j + 1) % 6) == 0)
                    {
                        if (i < 7)
                        {
                            Output_48Bit[i, j] = Input_32Bit[i + 1, 0];
                        }
                        else
                        {
                            Output_48Bit[i, j] = Input_32Bit[0, 0];
                        }
                    }
                    else if (((i * 6 + j + 1) % 6) == 1)
                    {
                        if (i > 0)
                        {
                            Output_48Bit[i, j] = Input_32Bit[i - 1, 3];
                        }
                        else
                        {
                            Output_48Bit[i, j] = Input_32Bit[7, 3];
                        }
                    }
                    else
                    {
                        Output_48Bit[i, j] = Input_32Bit[i, j - 1];
                    }
                }
            }

            return Output_48Bit;
        }
    }
}
