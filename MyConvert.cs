﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyDES
{
    public class MyConvert
    {
        private string Input = "";

        private byte[] Byte;

        private string Str = "";

        private string Binary = "";

        private string Base64 = "";

        private int[,,] BinaryMtx;

        public MyConvert()
        {

        }

        public MyConvert(string input)
        {
            this.Input = input;
        }

        public MyConvert(int[,,] input)
        {
            int de1 = input.GetLength(0);

            int de2 = input.GetLength(1);

            int de3 = input.GetLength(2);

            for (int i = 0; i < de1; i++)
            {
                for (int j = 0; j < de2; j++)
                {
                    for (int k = 0; k < de3; k++)
                    {
                        this.Input = this.Input.Insert(this.Input.Length, input[i, j, k].ToString());
                    }
                }
            }
        }

        private int[,,] BinaryToMtx()
        {
            this.Binary = this.Input;

            int num = this.Binary.Length % 64 == 0 ? this.Binary.Length / 64 : (this.Binary.Length / 64) + 1;

            BinaryMtx = new int[num, 8, 8];

            if (this.Binary.Length < 64 * num)
            {
                for (int i = this.Binary.Length; i < 64 * num; i++)
                {
                    this.Binary = this.Binary.Insert(this.Binary.Length, "0");
                }
            }

            for (int i = 0; i < num; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    for (int k = 0; k < 8; k++)
                    {
                        this.BinaryMtx[i, j, k] = this.Binary[64 * i + 8 * j + k] - '0';
                    }
                }
            }

            return this.BinaryMtx;
        }

        private string BinaryToStr()
        {
            this.Binary = this.Input;

            this.Byte = new byte[this.Input.Length / 8];

            for (int i = 0; i < this.Binary.Length / 8; i++)
            {
                this.Byte[i] = Convert.ToByte(this.Binary.Substring(i * 8, 8), 2);
            }

            this.Str = Encoding.UTF8.GetString(Byte);

            return this.Str;
        }

        private string BinaryToBase64()
        {
            this.Binary = this.Input;

            this.Byte = new byte[this.Input.Length / 8];

            for (int i = 0; i < this.Binary.Length / 8; i++)
            {
                this.Byte[i] = Convert.ToByte(this.Binary.Substring(i * 8, 8), 2);
            }

            this.Base64 = Convert.ToBase64String(this.Byte);

            return this.Base64;
        }

        private string MtxToBinary()
        {
            return this.Input;
        }

        public string MtxToStr()
        {
            this.Input = this.MtxToBase64();

            this.Str = this.Base64ToStr();

            return this.Str;
        }

        public string MtxToBase64()
        {
            this.Input = this.MtxToBinary();

            this.Base64 = this.BinaryToBase64();

            return this.Base64;
        }

        public int[,,] Base64ToMtx()
        {
            this.Input = this.Base64ToBinary();

            this.BinaryMtx = this.BinaryToMtx();

            return this.BinaryMtx;
        }

        private string Base64ToBinary()
        {
            this.Base64 = this.Input;

            this.Byte = Convert.FromBase64String(this.Base64);

            this.Binary="";

            foreach (var b in this.Byte)
            {
                this.Binary = this.Binary.Insert(this.Binary.Length, Convert.ToString(b, 2).PadLeft(8, '0'));
            }

            return this.Binary;
        }

        private string Base64ToStr()
        {
            this.Input = this.Base64ToBinary();

            this.Str = this.BinaryToStr();

            return this.Str;
        }

        public string StrToBase64()
        {
            this.Input = this.StrToBinary();

            this.Base64 = this.BinaryToBase64();

            return this.Base64;
        }

        private string StrToBinary()
        {
            this.Byte = Encoding.UTF8.GetBytes(Input);

            foreach (var b in this.Byte)
            {
                this.Binary = this.Binary.Insert(this.Binary.Length, Convert.ToString(b, 2).PadLeft(8, '0'));
            }

            return this.Binary;
        }

        public int[,,] StrToMtx()
        {
            this.Input = this.StrToBinary();

            this.BinaryMtx = this.BinaryToMtx();

            return this.BinaryMtx;
        }

        public int[,] Convert_Mtx8r4cToMtx4r8c(int[,] mtx8r4c)
        {
            int[,] Mtx4r8c = new int[4, 8];

            int row = 0;

            int col = 0;

            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    Mtx4r8c[i, j] = mtx8r4c[row, col];

                    col++;

                    if (col == 4)
                    {
                        col = 0;

                        row++;
                    }
                }
            }

            return Mtx4r8c;
        }

        public int[,] Convert_Mtx4r8cToMtx8r4c(int[,] mtx4r8c)
        {
            int[,] Mtx8r4c = new int[8, 4];

            int row = 0;

            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    Mtx8r4c[row, j % 4] = mtx4r8c[i, j];

                    if (j == 3 || j == 7)
                        row++;
                }
            }

            return Mtx8r4c;
        }
    }
}