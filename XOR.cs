﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyDES
{
    public class XOR
    {
        private int[,] Input_Mtx_1;

        private int[,] Input_Mtx_2;

        private int Input_Mtx_Rows;

        private int Input_Mtx_Cols;

        private int[,] Output_Mtx;

        public XOR(int[,] input_Mtx_1,int[,] input_Mtx_2)
        {
            this.Input_Mtx_1 = input_Mtx_1;

            this.Input_Mtx_2 = input_Mtx_2;

            Input_Mtx_Rows = Input_Mtx_1.GetLength(0);

            Input_Mtx_Cols = Input_Mtx_1.GetLength(1);

            this.Output_Mtx = new int[Input_Mtx_Rows, Input_Mtx_Cols];
        }

        public int[,] Get_XOR_ValMtx()
        {
            for (int i = 0; i < Input_Mtx_Rows; i++)
            {
                for (int j = 0; j < Input_Mtx_Cols; j++)
                {
                    if (this.Input_Mtx_1[i, j] == this.Input_Mtx_2[i, j])
                        this.Output_Mtx[i, j] = 0;
                    else
                        this.Output_Mtx[i, j] = 1;
                }
            }

            return this.Output_Mtx;
        }
    }
}
