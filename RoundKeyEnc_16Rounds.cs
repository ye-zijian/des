﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyDES
{
    public class RoundKeyEnc_16Rounds
    {

        private int Time = 0;

        private int[] Times_For_Digits = new int[16]
        { 1, 1, 2, 2, 2, 2, 2, 2, 1, 2, 2, 2, 2, 2, 2, 1 };

        private int[,,] Key_16Times = new int[16, 8, 6];

        private int[,] SeedKey_64Bit = new int[8, 8];

        private int[,] Key_Left_28Bit = new int[4, 7];

        private int[,] Key_Right_28Bit = new int[4, 7];

        public RoundKeyEnc_16Rounds(int[,] seedKey_64Bit)
        {
            this.SeedKey_64Bit = seedKey_64Bit;

            RoundKeyEnc_Pc_1 rpc_1 = new RoundKeyEnc_Pc_1(this.SeedKey_64Bit);

            rpc_1.Pc_1_Tra();

            this.Key_Left_28Bit = rpc_1.GetKey_Left_28Bit();

            this.Key_Right_28Bit = rpc_1.GetKey_Right_28Bit();

            Set_Key_16Times();
        }

        private void Set_Key_16Times()
        {
            this.Key_Left_28Bit = new Shift(this.Key_Left_28Bit, this.Times_For_Digits[this.Time]).ShiftOpe();

            this.Key_Right_28Bit = new Shift(this.Key_Right_28Bit, this.Times_For_Digits[this.Time]).ShiftOpe();

            Storekey();

            this.Time++;

            if (this.Time < 16)
                Set_Key_16Times();
        }

        private void Storekey()
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 6; j++)
                {
                    this.Key_16Times[this.Time, i, j] = new RoundKeyEnc_Pc_2(this.Key_Left_28Bit, this.Key_Right_28Bit).GetKey_48Bit()[i, j];
                }
            }
        }

        public int[,,] Get_Key_16Times()
        {
            return this.Key_16Times;
        }
    }
}
