﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyDES
{
    public class RoundFunc
    {
        private int[,] Right_32Bit = new int[8, 4];

        private int[,] Ex_Right_48Bit = new int[8, 6];

        private int[,] XOR_Right_48Bit = new int[8, 6];

        private int[,] SBox_Output_32Bit = new int[8, 4];

        private int[,] PBox_Output_32Bit = new int[8, 4];

        public RoundFunc(int[,] right_32Bit,int[,] round_Key_48Bit)
        {
            this.Right_32Bit = right_32Bit;

            this.Ex_Right_48Bit = new ExTra(this.Right_32Bit).Extend();

            this.XOR_Right_48Bit = new XOR(this.Ex_Right_48Bit, round_Key_48Bit).Get_XOR_ValMtx();

            this.SBox_Output_32Bit = new SBoxTra(this.XOR_Right_48Bit).Compress_48BitTo36Bit();

            this.PBox_Output_32Bit = new PBoxTra(this.SBox_Output_32Bit).GetPBox_Output_32Bit();
        }

        public int[,] GetRoundVal()
        {
            return this.PBox_Output_32Bit;
        }
    }
}
