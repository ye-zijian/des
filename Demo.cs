﻿using System;

namespace MyDES
{
    public class MyDES
    {
        static void Main(string[] args)
        {
            #region 最的测试矩阵。

            //int[,] TestRoundKeyEnc_16Rounds = new int[8, 8]
            //{
            //    {0,0,1,1,0,0,0,1 },
            //    {0,0,1,1,0,0,1,0 },
            //    {0,0,1,1,0,0,1,1 },
            //    {0,0,1,1,0,1,0,0 },
            //    {0,0,1,1,0,1,0,1 },
            //    {0,0,1,1,0,1,1,0 },
            //    {0,0,1,1,0,1,1,1 },
            //    {0,0,1,1,1,0,0,0 }
            //};

            //int[,] TestMtx = new int[8, 8]
            //{
            //    {0,0,1,1,0,0,0,0 },
            //    {0,0,1,1,0,0,0,1 },
            //    {0,0,1,1,0,0,1,0 },
            //    {0,0,1,1,0,0,1,1 },
            //    {0,0,1,1,0,1,0,0 },
            //    {0,0,1,1,0,1,0,1 },
            //    {0,0,1,1,0,1,1,0 },
            //    {0,0,1,1,0,1,1,1 }
            //};

            #endregion

            string PlaText1 = "19990924";

            string PlaText2 = @"
                                卡尔·马克思，全名卡尔·海因里希·马克思（德语：Karl Heinrich Marx，1818年5月5日－1883年3月14日），马克思主义的创始人之一，第一国际的组织者和领导者，
                                马克思主义政党的缔造者之一，全世界无产阶级和劳动人民的革命导师，无产阶级的精神领袖，国际共产主义运动的开创者。 [1-2] 
                                马克思是德国的思想家、政治学家、哲学家、经济学家、革命理论家、历史学家和社会学家。主要著作有《资本论》《共产党宣言》等。
                                马克思创立的广为人知的哲学思想为历史唯物主义，其最大的愿望是对于个人的全面而自由的发展。 [3]  马克思创立了经济理论《资本论》，
                                马克思确立他的阐述原则是“政治经济学批判”。马克思认为，这是“政治经济学原理”的东西。马克思认为资产阶级的灭亡和无产阶级的胜利是同样不可避免的。
                                他和恩格斯共同创立的马克思主义学说，被认为是指引全世界劳动人民为实现社会主义和共产主义理想而进行斗争的理论武器和行动指南。
                                ";

            string Key = "叶子健的密钥。";

            string CipText;

            CipText = new DES("DES_Enc", PlaText1, Key).Get_Text();

            Console.WriteLine(CipText);

            Console.WriteLine();

            string TestPlaText;

            TestPlaText = new DES("DES_Dec", CipText, Key).Get_Text();

            Console.WriteLine(TestPlaText);

            Console.ReadKey();

            #region 在写完加密算法之后又写了MyConvert类，使得可以任意输入UTF-8编码集中的字符作为要传输的明文或者密钥。测试过程及其繁琐。

            #endregion

            #region 测试解密能正常运行。

            #endregion

            #region 测试其余类及方法均无错，加密能正确运行。

            #endregion

            #region 测试轮函数（已通过，第16轮轮函数的计算逻辑出错，下列else中第三行语句的this.Pre_Left_32Bit写成了this.Right_32Bit导致逻辑错误。）

            //else
            //{
            //    this.Pre_Left_32Bit = Left_32Bit;

            //    this.Left_32Bit = new RoundFunc(this.Right_32Bit, this.GetOneKey_48Bit(Time)).GetRoundVal();

            //    this.Left_32Bit = new XOR(this.Left_32Bit, this.Pre_Left_32Bit).Get_XOR_ValMtx();

            //    this.Right_32Bit = this.Right_32Bit;
            //}

            #endregion

            #region 测试SBoxTra类中的Compress_48BitTo36Bit方法（已通过，错在下列第一行代码中的3-j写成了j导致转换成二进制时顺序相反）。

            //SBox_Output_32Bit[i, 3 - j] = SBox_OutputByNum[i] / (int)Math.Pow(2, j)

            //SBox_Output_32Bit[i, j] = SBox_OutputByNum[i] / (int)Math.Pow(2, j)

            #endregion

            #region 测试DES类中的Convert_Mtx4r8cToMtx8r4c方法（已通过，错在if (j == 3 || j == 7)写成if (j == 3)）。

            //int[,] TestConvert_Mtx4r8cToMtx8r4c_Mtx = new int[4, 8]
            //{
            //    {0,0,0,0,0,0,0,0 },
            //    {1,1,1,1,1,1,1,1 },
            //    {1,1,1,1,0,0,0,0 },
            //    {1,0,1,0,1,0,1,0 }
            //};

            //DES td = new DES();

            //int[,] TestMtx_1 = td.Convert_Mtx4r8cToMtx8r4c(TestConvert_Mtx4r8cToMtx8r4c_Mtx);

            //for (int i = 0; i < 8; i++)
            //{
            //    for (int j = 0; j < 4; j++)
            //    {
            //        Console.Write(TestMtx_1[i, j]);
            //    }

            //    Console.WriteLine();
            //}

            //Console.WriteLine();

            #endregion

            #region 测试密钥扩展方案RoundKeyEnc_16Rounds类（已通过，无错）。

            //Console.WriteLine(Convert.ToString(0x502CAC572AC2, 2).PadLeft(4, '0'));
            //Console.WriteLine(Convert.ToString(0x50ACA450A347, 2).PadLeft(4, '0'));
            //Console.WriteLine(Convert.ToString(0xD0AC26F6848C, 2).PadLeft(4, '0'));
            //Console.WriteLine(Convert.ToString(0xE0A6264837CB, 2).PadLeft(4, '0'));
            //Console.WriteLine(Convert.ToString(0xE096263EF025, 2).PadLeft(4, '0'));
            //Console.WriteLine(Convert.ToString(0xE09272625D62, 2).PadLeft(4, '0'));
            //Console.WriteLine(Convert.ToString(0xA4D2728CA93A, 2).PadLeft(4, '0'));
            //Console.WriteLine(Convert.ToString(0xA65352E55E50, 2).PadLeft(4, '0'));
            //Console.WriteLine(Convert.ToString(0x265353CB9A40, 2).PadLeft(4, '0'));
            //Console.WriteLine(Convert.ToString(0x2F5151D0C73C, 2).PadLeft(4, '0'));
            //Console.WriteLine(Convert.ToString(0x0F41D9191E8C, 2).PadLeft(4, '0'));
            //Console.WriteLine(Convert.ToString(0x1F4199D870B1, 2).PadLeft(4, '0'));
            //Console.WriteLine(Convert.ToString(0x1F0989236A2D, 2).PadLeft(4, '0'));
            //Console.WriteLine(Convert.ToString(0x1D288DB23992, 2).PadLeft(4, '0'));
            //Console.WriteLine(Convert.ToString(0x192C8CA50337, 2).PadLeft(4, '0'));
            //Console.WriteLine(Convert.ToString(0x512C8CA743C0, 2).PadLeft(4, '0'));
            //Console.WriteLine();

            //int[,] TestRoundKeyEnc_16Rounds = new int[8, 8]
            //{
            //    {0,0,1,1,0,0,0,1 },
            //    {0,0,1,1,0,0,1,0 },
            //    {0,0,1,1,0,0,1,1 },
            //    {0,0,1,1,0,1,0,0 },
            //    {0,0,1,1,0,1,0,1 },
            //    {0,0,1,1,0,1,1,0 },
            //    {0,0,1,1,0,1,1,1 },
            //    {0,0,1,1,1,0,0,0 }
            //};

            //RoundKeyEnc_16Rounds rke = new RoundKeyEnc_16Rounds(TestRoundKeyEnc_16Rounds);

            //int[,,] Key_16Times = rke.Get_Key_16Times();

            //for (int i = 0; i < 16; i++)
            //{
            //    for (int j = 0; j < 8; j++)
            //    {
            //        for (int k = 0; k < 6; k++)
            //        {
            //            Console.Write(Key_16Times[i, j, k]);
            //        }
            //    }
            //    Console.WriteLine();
            //}

            #endregion

            #region 测试移位操作Shift类（已通过，无错）。

            //int[,] TestShiftMtx = new int[4, 7]
            //{
            //    {1,1,1,1,1,1,1 },
            //    {0,0,0,0,0,0,0 },
            //    {1,1,1,1,1,1,1 },
            //    {0,0,0,0,0,0,0 }
            //};

            //TestShiftMtx = new Shift(TestShiftMtx, 27).ShiftOpe();


            //for (int i = 0; i < 4; i++)
            //{
            //    for (int j = 0; j < 7; j++)
            //    {
            //        Console.Write(TestShiftMtx[i, j]);

            //    }
            //}

            #endregion
        }
    }
}