﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyDES
{
    public class RoundKeyEnc_Pc_1
    {
        private int[,] Key_Input_64Bit = new int[8, 8];

        private int[,] Key_Output_56Bit = new int[8, 7];

        private int[,] Key_Left_28Bit = new int[4, 7];

        private int[,] Key_Right_28Bit = new int[4, 7];

        private int[,] Pc_1 = new int[8, 7]
        {
            {57,49,41,33,25,17,9  },
            {1 ,58,50,42,34,26,18 },
            {10,2 ,59,51,43,35,27 },
            {19,11,3 ,60,52,44,36 },
            {63,55,47,39,31,23,15 },
            {7 ,62,54,46,38,30,22 },
            {14,6 ,61,53,45,37,29 },
            {21,13,5 ,28,20,12,4  }
        };

        public RoundKeyEnc_Pc_1(int[,] key_Input_64Bit)
        {
            this.Key_Input_64Bit = key_Input_64Bit;
        }

        public void Pc_1_Tra()
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 7; j++)
                {
                    int row = (Pc_1[i, j] - 1) / 8;
                    int col = (Pc_1[i, j] - 1) % 8;
                    this.Key_Output_56Bit[i, j] = this.Key_Input_64Bit[row, col];
                }
            }
        }

        public int[,] GetKey_Left_28Bit()
        {
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 7; j++)
                {
                    this.Key_Left_28Bit[i, j] = this.Key_Output_56Bit[i, j];
                }
            }

            return this.Key_Left_28Bit;
        }

        public int[,] GetKey_Right_28Bit()
        {
            for (int i = 4; i < 8; i++)
            {
                for (int j = 0; j < 7; j++)
                {
                    this.Key_Right_28Bit[i - 4, j] = this.Key_Output_56Bit[i, j];
                }
            }

            return this.Key_Right_28Bit;
        }
    }
}
