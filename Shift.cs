﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyDES
{
    public class Shift
    {
        private int[,] Input_key_28Bit = new int[4, 7];

        private int[,] Output_key_28Bit = new int[4, 7];

        private int Times;

        public Shift(int[,] input_key_28Bit, int times)
        {
            this.Input_key_28Bit = input_key_28Bit;
            this.Times = times;
        }

        public int[,] ShiftOpe()
        {
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 7; j++)
                {
                    if (j == 6)
                    {
                        if (i == 3)
                            Output_key_28Bit[i, j] = Input_key_28Bit[0, 0];
                        else
                            Output_key_28Bit[i, j] = Input_key_28Bit[i + 1, 0];
                    }
                    else
                        Output_key_28Bit[i, j] = Input_key_28Bit[i, j + 1];
                }
            }

            this.Times--;

            if (this.Times > 0)
                return new Shift(this.Output_key_28Bit, this.Times).ShiftOpe();
            else
                return this.Output_key_28Bit;
        }
    }
}
