﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyDES
{
    public class DES
    {
        private int[,] Pre_Left_32Bit = new int[8, 4];

        private int[,] Left_32Bit = new int[8, 4];

        private int[,] Right_32Bit = new int[8, 4];

        private int[,,] Key_16Times = new int[16, 8, 6];

        private int[,] One_Key_48Bit = new int[8, 6];

        private int[,] Output_64Bit = new int[8, 8];

        private int[,,] Text;

        private int[,,] BinaryMtx;

        private int[,] One_BinaryMtx_64Bit = new int[8, 8];

        private int Time = 0;

        private int De = 0;

        private string Model;

        public DES(string model, string text, string key)
        {
            if (model == "DES_Enc")
            {
                text = new MyConvert(text).StrToBase64();
            }

            int[,,] seedKey = new MyConvert(key).StrToMtx();

            int key_De;

            if (seedKey.GetLength(0) > 1)
            {
                Console.WriteLine("请注意，你的密钥长度已近超过了64Bit，达到了" + seedKey.Length + "Bit，请你在1到" + seedKey.GetLength(0) + "组密钥中选择一个来作为你的64Bit密钥组。请牢记你选择的数字。");
                Console.WriteLine();
                Console.Write("请输入数字：");
                key_De = Console.ReadKey().KeyChar - 49;
                Console.WriteLine();
                Console.WriteLine();
            }

            else
                key_De = 0;

            int[,] seedKeyMtx = new int[8, 8];

            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    seedKeyMtx[i, j] = seedKey[key_De, i, j];
                }
            }

            RoundKeyEnc_16Rounds rke = new RoundKeyEnc_16Rounds(seedKeyMtx);

            this.Model = model;

            this.Key_16Times = rke.Get_Key_16Times();

            this.BinaryMtx = new MyConvert(text).Base64ToMtx();

            this.Text = new int[this.BinaryMtx.GetLength(0), 8, 8];

            for (int i = 0; i < this.BinaryMtx.GetLength(0); i++)
            {
                IpTra ip = new IpTra(GetOneBinaryMtx_64Bit(i));

                this.Left_32Bit = new MyConvert().Convert_Mtx4r8cToMtx8r4c(ip.GetDop_Left_32Bit());

                this.Right_32Bit = new MyConvert().Convert_Mtx4r8cToMtx8r4c(ip.GetDop_Right_32Bit());

                this.Func_16Rounds(model);

                this.De++;

                this.Time = 0;
            }

            #region 之前的DES构造器代码，随着轮函数调用代码的改进而改进了。

            //switch (model)
            //{
            //    case "DES_Enc":

            //        RoundKeyEnc_16Rounds rke_1 = new RoundKeyEnc_16Rounds(seedKey);

            //        this.Key_16Times = rke_1.Get_Key_16Times();

            //        IpTra ip_1 = new IpTra(text);

            //        this.Left_32Bit = this.Convert_Mtx4r8cToMtx8r4c(ip_1.GetDop_Left_32Bit());

            //        this.Right_32Bit = this.Convert_Mtx4r8cToMtx8r4c(ip_1.GetDop_Right_32Bit());

            //        this.Func_16Rounds(model);

            //        break;

            //    case "DES_Dec":

            //        RoundKeyEnc_16Rounds rke_2 = new RoundKeyEnc_16Rounds(seedKey);

            //        this.Key_16Times = rke_2.Get_Key_16Times();

            //        IpTra ip_2 = new IpTra(text);

            //        this.Left_32Bit = this.Convert_Mtx4r8cToMtx8r4c(ip_2.GetDop_Left_32Bit());

            //        this.Right_32Bit = this.Convert_Mtx4r8cToMtx8r4c(ip_2.GetDop_Right_32Bit());

            //        this.Func_16Rounds(model);

            //        break;

            //    default:

            //        Console.WriteLine("你输入了个什么东西？");

            //        break;
            //}

            #endregion
        }

        private void Func_16Rounds(string model)
        {
            int index;

            if (model == "DES_Dec")

                index = 15 - this.Time;

            else

                index = this.Time;

            this.Pre_Left_32Bit = Left_32Bit;

            this.Left_32Bit = this.Right_32Bit;

            this.Right_32Bit = new RoundFunc(this.Right_32Bit, this.GetOneKey_48Bit(index)).GetRoundVal();

            this.Right_32Bit = new XOR(this.Pre_Left_32Bit, this.Right_32Bit).Get_XOR_ValMtx();

            this.Time++;

            if (this.Time < 15)

                this.Func_16Rounds(model);

            else
            {
                if (model == "DES_Dec")

                    index = 15 - this.Time;

                else

                    index = this.Time;

                this.Pre_Left_32Bit = Left_32Bit;

                this.Left_32Bit = new RoundFunc(this.Right_32Bit, this.GetOneKey_48Bit(index)).GetRoundVal();

                this.Left_32Bit = new XOR(this.Left_32Bit, this.Pre_Left_32Bit).Get_XOR_ValMtx();

                this.Left_32Bit = new MyConvert().Convert_Mtx8r4cToMtx4r8c(this.Left_32Bit);

                this.Right_32Bit = new MyConvert().Convert_Mtx8r4cToMtx4r8c(this.Right_32Bit);

                StoreText(new AntiIpTra(this.Get16RoundFuncVal_64Bit()).GetAntiIpTraVal());
            }
        }

        #region 之前的轮函数调用的代码，因为重复过多，废弃不用，已经改进成一个方法了。

        //private void Enc_16Rounds()
        //{
        //    this.Pre_Left_32Bit = Left_32Bit;

        //    this.Left_32Bit = this.Right_32Bit;

        //    this.Right_32Bit = new RoundFunc(this.Right_32Bit, this.GetOneKey_48Bit(Time)).GetRoundVal();

        //    this.Right_32Bit = new XOR(this.Pre_Left_32Bit, this.Right_32Bit).Get_XOR_ValMtx();

        //    this.Time++;

        //    if (this.Time < 15)

        //        Enc_16Rounds();

        //    else
        //    {
        //        this.Pre_Left_32Bit = Left_32Bit;

        //        this.Left_32Bit = new RoundFunc(this.Right_32Bit, this.GetOneKey_48Bit(Time)).GetRoundVal();

        //        this.Left_32Bit = new XOR(this.Left_32Bit, this.Pre_Left_32Bit).Get_XOR_ValMtx();

        //        this.Left_32Bit = Convert_Mtx8r4cToMtx4r8c(this.Left_32Bit);

        //        this.Right_32Bit = Convert_Mtx8r4cToMtx4r8c(this.Right_32Bit);

        //        this.CipText = new AntiIpTra(this.Get16EncVal_64Bit()).GetAntiIpRepVal();
        //    }
        //}

        //private void Dec_16Rounds()
        //{
        //    this.Pre_Left_32Bit = Left_32Bit;

        //    this.Left_32Bit = this.Right_32Bit;

        //    this.Right_32Bit = new RoundFunc(this.Right_32Bit, this.GetOneKey_48Bit(15 - Time)).GetRoundVal();

        //    this.Right_32Bit = new XOR(this.Pre_Left_32Bit, this.Right_32Bit).Get_XOR_ValMtx();

        //    this.Time++;

        //    if (this.Time < 15)

        //        Dec_16Rounds();

        //    else
        //    {
        //        this.Pre_Left_32Bit = Left_32Bit;

        //        this.Left_32Bit = new RoundFunc(this.Right_32Bit, this.GetOneKey_48Bit(15 - Time)).GetRoundVal();

        //        this.Left_32Bit = new XOR(this.Left_32Bit, this.Pre_Left_32Bit).Get_XOR_ValMtx();

        //        this.Left_32Bit = Convert_Mtx8r4cToMtx4r8c(this.Left_32Bit);

        //        this.Right_32Bit = Convert_Mtx8r4cToMtx4r8c(this.Right_32Bit);

        //        this.PlaText = new AntiIpTra(this.Get16EncVal_64Bit()).GetAntiIpRepVal();
        //    }
        //}

        #endregion

        private int[,] Get16RoundFuncVal_64Bit()
        {
            for (int i = 0; i < 4; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    this.Output_64Bit[i, j] = this.Left_32Bit[i, j];
                    this.Output_64Bit[i + 4, j] = this.Right_32Bit[i, j];
                }
            }

            return this.Output_64Bit;
        }

        private int[,] GetOneKey_48Bit(int time)
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 6; j++)
                {
                    this.One_Key_48Bit[i, j] = this.Key_16Times[time, i, j];
                }
            }

            return this.One_Key_48Bit;
        }

        private int[,] GetOneBinaryMtx_64Bit(int de)
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    this.One_BinaryMtx_64Bit[i, j] = this.BinaryMtx[de, i, j];
                }
            }

            return this.One_BinaryMtx_64Bit;
        }

        private void StoreText(int[,] text_64Bit)
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 8; j++)
                {
                    this.Text[De, i, j] = text_64Bit[i, j];
                }
            }
        }

        public string Get_Text()
        {
            if (this.Model == "DES_Enc")
            {
                return new MyConvert(this.Text).MtxToBase64();
            }

            return new MyConvert(this.Text).MtxToStr();
        }
    }
}
