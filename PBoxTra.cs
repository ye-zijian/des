﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MyDES
{
    public class PBoxTra
    {
        private int[,] PBox_Input_32Bit = new int[8, 4];

        private int[,] PBox_Output_32Bit = new int[8, 4];

        private int[,] PBox = new int[8, 4]
        {
            {16,7,20,21 },
            {29,12,28,17 },
            {1,15,23,26 },
            {5,18,31,10 },
            {2,8,24,14 },
            {32,27,3,9 },
            {19,13,30,6 },
            {22,11,4,25 }
        };

        public PBoxTra(int[,] pBox_Input_32Bit)
        {
            this.PBox_Input_32Bit = pBox_Input_32Bit;
        }

        public int[,] GetPBox_Output_32Bit()
        {
            for (int i = 0; i < 8; i++)
            {
                for (int j = 0; j < 4; j++)
                {
                    int row = (PBox[i, j] - 1) / 4;
                    int col = (PBox[i, j] - 1) % 4;
                    this.PBox_Output_32Bit[i, j] = this.PBox_Input_32Bit[row, col];
                }
            }

            return this.PBox_Output_32Bit;
        }
    }
}
